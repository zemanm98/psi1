
import socket
import binascii

def main():
    MCAST_GRP = '239.255.255.250' #IP address for SSDP communication
    MCAST_PORT = 1900 # SSDP port
    SSDP_MSG = str.encode("M-SEARCH * HTTP/1.1\r\n" # SSDP message
                          "HOST: 239.255.255.250:1900\r\n"
                          "MAN: \"ssdp:discover\"\r\n"
                          "MX: 2\r\n"
                          "ST: ssdp:all\r\n\r\n")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(SSDP_MSG, (MCAST_GRP, MCAST_PORT))
    for i in range(0, 1): # print first recieved message
    	print(sock.recvfrom(1000))
    	print("\n\n")


if __name__ == '__main__':
    main()
