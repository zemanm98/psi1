# PSI1 - Multicast UDP client

Task was to create multicast client, that sends SSDP protocol packet for discovering UPnP devices in network.
I went with python since i already had some experience with sockets in python.

# BUILD
All the neccesary code is written in main.py script run by command
```bash
python3 main.py
```

I tested script on our home network with smart TV Sony Bravia and got answer looking like this

<img src="img/wireshark.jpg">

# FIREWALL
Application is able to send SSDP protocol packet to network but can't recieve any response if firewall is not configured for it. So for the appliaction to run properly you have to either turn off firewall with
```bash
sudo ufw disable
```
run the script and turn it on with
```bash
sudo ufw enable
```
but this might lead to security breach. Another option is to add rule to firewall so it lets ssdp packets through. SSDP protocol sends packets to port 1900.

# WIRESHARK
When you run the aplication and look in listening wireshark you should see output looking like this

<img src="img/terminal.jpg">
